var app = angular.module('StarterApp', ['ngMaterial']);

app.controller('AppCtrl', function($scope) {
  $scope.channelPairs = [{
    name: "Channel pair 1",
    color: "green",
    lambda: 1230,
    disabled: false
  }, {
    name: "Channel pair 2",
    color: "red",
    lambda: 760,
    disabled: false
  }];
  $scope.allDisabled = false;
  
  $scope.dummy = 'dummy';

  $scope.simulate = function() {
    $scope.allDisabled = !$scope.allDisabled;
   console.log('test'); $scope.channelPairs.forEach(function(cp) {
       cp.disabled = !cp.disabled;

    });
  }
  
  $scope.labels = ["1" , "2", "3", "4", "5"];
  $scope.series = ['Bytes in', 'Bytes out'];
  $scope.data = [
    [65, 59, 80, 81, 56],
    [28, 48, 40, 19, 86]
  ];

});