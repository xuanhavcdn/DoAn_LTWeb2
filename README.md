# Homework 2 - *angularjs material*

Danh sách sinh viên: **1312157 - Cao Xuân Hà**

URL: ****

## Chức năng

Danh sách chức năng **bắt buộc**:

* [x] know your education background
* [x] see your social profiles like : Facebook, LinkedIn
* [x] see your skills set
* [x] see a unique and creative design
* [x] see the results of your projects in the past
* [x] see your hobbies and social relationships
* [x] Enter information to a contact form

Danh sách chức năng **phụ**:

* [x] see proofs that website build by Angularjs 1.x
* [x] see proofs that website hosted on Firebase
* [x] see proofs that the design of website is Material Design
* [x] cannot find proofs that you copy code from another students
## Notes

Describe any challenges encountered while building the app.

## License

    Copyright [yyyy] [name of copyright owner]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
